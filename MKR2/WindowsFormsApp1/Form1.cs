﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void завдання1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 frm1 = new Form1();
            frm1.ShowDialog();
        }

        private void завдання2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
            private int CountFirstDigitChanges(string sequenceText)
            {
                string[] numbers = sequenceText.Split(' ');
                int count = 0;

                if (numbers.Length > 0)
                {
                    char firstDigit = numbers[0][0];

                    for (int i = 1; i < numbers.Length; i++)
                    {
                        if (numbers[i][0] != firstDigit)
                        {
                            count++;
                            firstDigit = numbers[i][0];
                        }
                    }
                }

                return count;
            }
        

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string sequenceText = sequenceTextBox.Text;
            int count = CountFirstDigitChanges(sequenceText);
            resultLabel.Text = "Кількість змін першої цифри: " + count.ToString();
        
        }
    }

}

