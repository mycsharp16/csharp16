﻿namespace WindowsFormsApp1
{
    internal class Book
    {
        private string v1;
        private string v2;
        private int v3;

        public Book(string v1, string v2, int v3)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }

        public Book(string v1, string v2, int v3, int v, int v4)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }
    }
}