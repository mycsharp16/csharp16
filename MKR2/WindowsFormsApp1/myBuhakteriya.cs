﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
   
        [Serializable]
        internal class Book1
        {
            public string Name { get; set; }
            public string Position { get; set; }
            public double Salary { get; set; }
            public double Children { get; set; }
            public double Experience { get; set; }
            public Book1(string _name, string _position, double _salary, double _children, double _experience)
            {
                Name = _name;
                Position = _position;
                Salary = _salary;
                Children = _children;
                Experience = _experience;
            }
        }
    
}
