﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {

        List<Book1> BookList;
        BindingSource BindSource;
        public Form2()
        {
            InitializeComponent();
        }

        private void добавитиНовийЗаписаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.ShowDialog();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //3.Створюємо об"єкт колецію-даних
            BookList = new List<Book1>
            {
                new Book1("Півкач", "Програміст", 2010, 2, 5),
                new Book1("Вогар", "Програміст", 2010, 3, 5),
                new Book1("Коваль", "Програміст", 2010, 2, 7),
                new Book1("Лях", "Програміст", 2340, 1, 4),
                new Book1("Гриценко", "Програміст", 7010, 3, 10)
            };

            BindSource = new BindingSource();
            BindSource.DataSource = BookList;
            Buhalteriya.DataSource = BindSource;

            Buhalteriya.Columns["Name"].HeaderText = "Прізвище";
            Buhalteriya.Columns["Position"].HeaderText = "Посада";
            Buhalteriya.Columns["Salary"].HeaderText = "Зарплата";
            Buhalteriya.Columns["Children"].HeaderText = "Кількість дітей";
            Buhalteriya.Columns["Experience"].HeaderText = "Стаж";
        }

        
        List<Book1> BookListFiltered;
        
        BindingSource BindSourceFiltered;
        private void button1_Click(object sender, EventArgs e)
        {
                string author = searcName.Text;
                BookListFiltered = BookList.FindAll(book => book.Name.Contains(author));

                BindSourceFiltered = new BindingSource();
                BindSourceFiltered.DataSource = BookListFiltered;

                searchResultGrid.DataSource = BindSourceFiltered;
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, BookList);
                fs.Close();
            }
        }

        private void відкритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                BindSource.Clear();
                foreach (Book1 b in (List<Book1>)bf.Deserialize(fs))
                    BindSource.Add(b);
                fs.Close();
            }
        }

        private void експортВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Створюємо екземпляр додатку
            Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

            //Відображаємо додаток (не обов"язково)
            excelApp.Visible = true;

            //Задаємо кількість листів у книзі
            excelApp.SheetsInNewWorkbook = 1;

            //Створюємо робочу книгу
            Excel.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);

            //Відключаємо можливість відображення повідомлень
            excelApp.DisplayAlerts = false;

            //Отримуємо посилання на перший лист(нумерація від 1)
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);

            //Записуємо дані у  лист
            int offsetRow = 1;
            //Записуємо назви стовпців
            for (int j = 0; j < Buhalteriya.ColumnCount; j++)
            {
                sheet.Cells[offsetRow, j + 1] = Buhalteriya.Columns[j].HeaderText;
            }

            //Записуємо дані 
            for (int i = 0; i < Buhalteriya.RowCount; i++)
            {
                for (int j = 0; j < Buhalteriya.ColumnCount; j++)
                    sheet.Cells[offsetRow + i + 1, j + 1] = Buhalteriya[j, i].Value;
            }

            //Задаємо діапазон клітинок
            Excel.Range range1 = sheet.Range[sheet.Cells[offsetRow, 1], sheet.Cells[offsetRow, Buhalteriya.ColumnCount]];
            //Ім"я шрифту
            range1.Cells.Font.Name = "Arial";
            range1.Cells.Font.Size = 14;
            range1.Cells.Font.Color = ColorTranslator.ToOle(Color.White);
        }
    }
}

