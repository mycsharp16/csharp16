﻿namespace Labka22
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelInterval = new System.Windows.Forms.Label();
            this.numericUpDownA = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownB = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownInterval = new System.Windows.Forms.NumericUpDown();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.завданняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.операціїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вЯкомуКвадратіБільшеТочокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.середнєЗначенняФункціїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найбільшеВідємнеЗначенняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найменшеДодатнеЗначенняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завдання1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завдання2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завдання3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(35, 97);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(15, 16);
            this.labelA.TabIndex = 0;
            this.labelA.Text = "a";
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(35, 137);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(15, 16);
            this.labelB.TabIndex = 1;
            this.labelB.Text = "b";
            // 
            // labelInterval
            // 
            this.labelInterval.AutoSize = true;
            this.labelInterval.Location = new System.Drawing.Point(21, 168);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(50, 16);
            this.labelInterval.TabIndex = 2;
            this.labelInterval.Text = "Interval";
            // 
            // numericUpDownA
            // 
            this.numericUpDownA.Location = new System.Drawing.Point(101, 91);
            this.numericUpDownA.Name = "numericUpDownA";
            this.numericUpDownA.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownA.TabIndex = 3;
            this.numericUpDownA.ValueChanged += new System.EventHandler(this.numericUpDownA_ValueChanged_1);
            // 
            // numericUpDownB
            // 
            this.numericUpDownB.Location = new System.Drawing.Point(101, 131);
            this.numericUpDownB.Name = "numericUpDownB";
            this.numericUpDownB.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownB.TabIndex = 4;
            this.numericUpDownB.ValueChanged += new System.EventHandler(this.numericUpDownB_ValueChanged_1);
            // 
            // numericUpDownInterval
            // 
            this.numericUpDownInterval.Location = new System.Drawing.Point(101, 166);
            this.numericUpDownInterval.Name = "numericUpDownInterval";
            this.numericUpDownInterval.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownInterval.TabIndex = 5;
            this.numericUpDownInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownInterval.ValueChanged += new System.EventHandler(this.numericUpDownInterval_ValueChanged_1);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(227, 56);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.Name = "Series2";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(531, 300);
            this.chart1.TabIndex = 5;
            this.chart1.Text = "chart1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.завданняToolStripMenuItem,
            this.операціїToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // завданняToolStripMenuItem
            // 
            this.завданняToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.завдання1ToolStripMenuItem,
            this.завдання2ToolStripMenuItem,
            this.завдання3ToolStripMenuItem});
            this.завданняToolStripMenuItem.Name = "завданняToolStripMenuItem";
            this.завданняToolStripMenuItem.Size = new System.Drawing.Size(93, 24);
            this.завданняToolStripMenuItem.Text = "Завдання ";
            // 
            // операціїToolStripMenuItem
            // 
            this.операціїToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem,
            this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem,
            this.вЯкомуКвадратіБільшеТочокToolStripMenuItem,
            this.середнєЗначенняФункціїToolStripMenuItem,
            this.найбільшеВідємнеЗначенняToolStripMenuItem,
            this.найменшеДодатнеЗначенняToolStripMenuItem,
            this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem,
            this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem,
            this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem,
            this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem});
            this.операціїToolStripMenuItem.Name = "операціїToolStripMenuItem";
            this.операціїToolStripMenuItem.Size = new System.Drawing.Size(85, 24);
            this.операціїToolStripMenuItem.Text = "Операції";
            // 
            // чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem
            // 
            this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Name = "чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem";
            this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Text = "Чи є спадною та знайти найбільше та найменше значення";
            this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Click += new System.EventHandler(this.чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem_Click);
            // 
            // чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem
            // 
            this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem.Name = "чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem";
            this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem.Text = "Чи перетинає графік функції вісь ОХ";
            this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem.Click += new System.EventHandler(this.чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem_Click_1);
            // 
            // вЯкомуКвадратіБільшеТочокToolStripMenuItem
            // 
            this.вЯкомуКвадратіБільшеТочокToolStripMenuItem.Name = "вЯкомуКвадратіБільшеТочокToolStripMenuItem";
            this.вЯкомуКвадратіБільшеТочокToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.вЯкомуКвадратіБільшеТочокToolStripMenuItem.Text = "В якому квадраті більше точок";
            this.вЯкомуКвадратіБільшеТочокToolStripMenuItem.Click += new System.EventHandler(this.вЯкомуКвадратіБільшеТочокToolStripMenuItem_Click);
            // 
            // середнєЗначенняФункціїToolStripMenuItem
            // 
            this.середнєЗначенняФункціїToolStripMenuItem.Name = "середнєЗначенняФункціїToolStripMenuItem";
            this.середнєЗначенняФункціїToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.середнєЗначенняФункціїToolStripMenuItem.Text = "\"середнє значення\" функції";
            this.середнєЗначенняФункціїToolStripMenuItem.Click += new System.EventHandler(this.середнєЗначенняФункціїToolStripMenuItem_Click_1);
            // 
            // найбільшеВідємнеЗначенняToolStripMenuItem
            // 
            this.найбільшеВідємнеЗначенняToolStripMenuItem.Name = "найбільшеВідємнеЗначенняToolStripMenuItem";
            this.найбільшеВідємнеЗначенняToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.найбільшеВідємнеЗначенняToolStripMenuItem.Text = "найбільше від\'ємне значення";
            this.найбільшеВідємнеЗначенняToolStripMenuItem.Click += new System.EventHandler(this.найбільшеВідємнеЗначенняToolStripMenuItem_Click_1);
            // 
            // найменшеДодатнеЗначенняToolStripMenuItem
            // 
            this.найменшеДодатнеЗначенняToolStripMenuItem.Name = "найменшеДодатнеЗначенняToolStripMenuItem";
            this.найменшеДодатнеЗначенняToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.найменшеДодатнеЗначенняToolStripMenuItem.Text = "Найменше додатне значення";
            this.найменшеДодатнеЗначенняToolStripMenuItem.Click += new System.EventHandler(this.найменшеДодатнеЗначенняToolStripMenuItem_Click_1);
            // 
            // чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem
            // 
            this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Name = "чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem";
            this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Text = "чи є зростаючою та знайти найбільше та найменше значення";
            this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem.Click += new System.EventHandler(this.чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem_Click);
            // 
            // наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem
            // 
            this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem.Name = "наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem";
            this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem.Text = "наближено знайти точку перетину графіка фінкції і віссю ОХ";
            this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem.Click += new System.EventHandler(this.наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem_Click);
            // 
            // відобразитиСиметричноВідносноОсіОХToolStripMenuItem
            // 
            this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem.Name = "відобразитиСиметричноВідносноОсіОХToolStripMenuItem";
            this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem.Text = "відобразити симетрично відносно осі ОХ";
            this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem.Click += new System.EventHandler(this.відобразитиСиметричноВідносноОсіОХToolStripMenuItem_Click_1);
            // 
            // відобразитиСиметричноВідносноОсіОУToolStripMenuItem
            // 
            this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem.Name = "відобразитиСиметричноВідносноОсіОУToolStripMenuItem";
            this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem.Size = new System.Drawing.Size(526, 26);
            this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem.Text = "відобразити симетрично відносно осі ОУ";
            this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem.Click += new System.EventHandler(this.відобразитиСиметричноВідносноОсіОУToolStripMenuItem_Click_1);
            // 
            // завдання1ToolStripMenuItem
            // 
            this.завдання1ToolStripMenuItem.Name = "завдання1ToolStripMenuItem";
            this.завдання1ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.завдання1ToolStripMenuItem.Text = "Завдання 1";
            this.завдання1ToolStripMenuItem.Click += new System.EventHandler(this.завдання1ToolStripMenuItem_Click_1);
            // 
            // завдання2ToolStripMenuItem
            // 
            this.завдання2ToolStripMenuItem.Name = "завдання2ToolStripMenuItem";
            this.завдання2ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.завдання2ToolStripMenuItem.Text = "Завдання 2";
            this.завдання2ToolStripMenuItem.Click += new System.EventHandler(this.завдання2ToolStripMenuItem_Click_1);
            // 
            // завдання3ToolStripMenuItem
            // 
            this.завдання3ToolStripMenuItem.Name = "завдання3ToolStripMenuItem";
            this.завдання3ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.завдання3ToolStripMenuItem.Text = "Завдання 3";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.numericUpDownInterval);
            this.Controls.Add(this.numericUpDownB);
            this.Controls.Add(this.numericUpDownA);
            this.Controls.Add(this.labelInterval);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelInterval;
        private System.Windows.Forms.NumericUpDown numericUpDownA;
        private System.Windows.Forms.NumericUpDown numericUpDownB;
        private System.Windows.Forms.NumericUpDown numericUpDownInterval;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem завданняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завдання1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завдання2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завдання3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem операціїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чиЄСпадноюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чиПеретинаєГрафікФункціїВісьОХToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вЯкомуКвадратіБільшеТочокToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem середнєЗначенняФункціїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найбільшеВідємнеЗначенняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найменшеДодатнеЗначенняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чиЄЗростаючоюТаЗнайтиНайбільшеТаНайменшеЗначенняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наближеноЗнайтиТочкуПеретинуГрафікаФінкціїІВіссюОХToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem відобразитиСиметричноВідносноОсіОХToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem відобразитиСиметричноВідносноОсіОУToolStripMenuItem;
    }
}