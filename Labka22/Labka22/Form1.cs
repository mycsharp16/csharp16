﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labka22
{
    public partial class Form1 : Form
    {
        int[] arr = new int[1];
        int i = 0;
        int amount_of_pairs = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label5.Text = $"Кількість елементів масиву = ({Convert.ToString(arr.Length)})";
            label6.Text = "Кількість арифметичних послідовностей = 0";

        }

        private void завдання2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.ShowDialog();
        }

        private void завдання3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.ShowDialog();
        }

            private void button1_Click(object sender, EventArgs e)
        {
            if (i >= arr.Length)
            {
            }
            else
            {
                arr[i] = Convert.ToInt32(numericUpDown1.Value);
                i++;
                label3.Text = $"[ {string.Join(", ", arr)} ]";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            i = 0;
            label3.Text = "[ ]";
            arr = new int[Convert.ToInt32(numericUpDown2.Value)];
            label5.Text = $"Кількість елементів масиву = ({Convert.ToString(arr.Length)})";

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int a = 0; a < arr.Length; a++)
            {
                if (arr[a + 1] - arr[a] == arr[a + 2] - arr[a + 1])
                {
                    amount_of_pairs++;
                }
                if (a + 2 == arr.Length - 1)
                {
                    break;
                }
            }
            label6.Text = $"Кількість арифметичних послідовностей = {amount_of_pairs}";
        }

        private void завдання1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.ShowDialog();
        }

        private void завдання3ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.ShowDialog();
        }
    }
}
