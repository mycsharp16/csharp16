﻿namespace Labka22
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.згенеруватиРандомніЧислаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завданняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завдання1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завдання2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завдання3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(304, 109);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(390, 202);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(301, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(434, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сума елементів, роздашованих нижче головної діагоналі = ()";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "К-сть рядків / стовпців";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(45, 184);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(178, 35);
            this.button1.TabIndex = 3;
            this.button1.Text = "Знайти суму";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(55, 129);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(145, 22);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.завданняToolStripMenuItem,
            this.згенеруватиРандомніЧислаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // згенеруватиРандомніЧислаToolStripMenuItem
            // 
            this.згенеруватиРандомніЧислаToolStripMenuItem.Name = "згенеруватиРандомніЧислаToolStripMenuItem";
            this.згенеруватиРандомніЧислаToolStripMenuItem.Size = new System.Drawing.Size(224, 24);
            this.згенеруватиРандомніЧислаToolStripMenuItem.Text = "Згенерувати рандомні числа";
            this.згенеруватиРандомніЧислаToolStripMenuItem.Click += new System.EventHandler(this.згенеруватиРандомноЦислаToolStripMenuItem_Click);
            // 
            // завданняToolStripMenuItem
            // 
            this.завданняToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.завдання1ToolStripMenuItem,
            this.завдання2ToolStripMenuItem,
            this.завдання3ToolStripMenuItem});
            this.завданняToolStripMenuItem.Name = "завданняToolStripMenuItem";
            this.завданняToolStripMenuItem.Size = new System.Drawing.Size(89, 24);
            this.завданняToolStripMenuItem.Text = "Завдання";
            // 
            // завдання1ToolStripMenuItem
            // 
            this.завдання1ToolStripMenuItem.Name = "завдання1ToolStripMenuItem";
            this.завдання1ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.завдання1ToolStripMenuItem.Text = "Завдання1";
            this.завдання1ToolStripMenuItem.Click += new System.EventHandler(this.завдання1ToolStripMenuItem_Click_1);
            // 
            // завдання2ToolStripMenuItem
            // 
            this.завдання2ToolStripMenuItem.Name = "завдання2ToolStripMenuItem";
            this.завдання2ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.завдання2ToolStripMenuItem.Text = "Завдання2";
            this.завдання2ToolStripMenuItem.Click += new System.EventHandler(this.завдання2ToolStripMenuItem_Click);
            // 
            // завдання3ToolStripMenuItem
            // 
            this.завдання3ToolStripMenuItem.Name = "завдання3ToolStripMenuItem";
            this.завдання3ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.завдання3ToolStripMenuItem.Text = "Завдання3";
            this.завдання3ToolStripMenuItem.Click += new System.EventHandler(this.завдання3ToolStripMenuItem_Click_1);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem згенеруватиРандомніЧислаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завданняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завдання1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завдання2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завдання3ToolStripMenuItem;
    }
}