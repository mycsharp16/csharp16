﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labka22
{
    public partial class Form2 : Form
    {
        System.Random random = new System.Random();
        int[,] arr;
        int i = 0;
        int sum = 0;

        public Form2()
        {
            InitializeComponent();
        }

        


            private void згенеруватиРандомноЦислаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                for (int a = 0; a < dataGridView1.RowCount; a++)
                {
                    dataGridView1.Rows[i].Cells[a].Value = random.Next(-100, 100);
                }
            }

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = 1;
            dataGridView1.RowCount = 1;
            numericUpDown1.Value = dataGridView1.ColumnCount;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double sum = 0;
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                for (int a = 0; a < dataGridView1.RowCount; a++)
                {
                    if (a < i)
                    {
                        sum += Convert.ToInt32(dataGridView1.Rows[i].Cells[a].Value);
                    }
                }
            }
            label1.Text = $"Сума елементів, розташованих нижче головної діагоналі = ({sum})";

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = (int)numericUpDown1.Value;
            dataGridView1.RowCount = (int)numericUpDown1.Value;

        }

        private void завдання2ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void завдання3ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.ShowDialog();
        }

        private void завдання1ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form1 frm1 = new Form1();
            frm1.ShowDialog();
        }
    }
}
