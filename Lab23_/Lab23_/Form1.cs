﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab23_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 1;
            numericUpDown1.Value = 1;
        }

        private void відкритиToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                WorkWithDataGridView.WriteToGrid(dataGridView1, WorkWithTxtFiles.readArrayFromString(openFileDialog1.FileName));
                numericUpDown1.Value = dataGridView1.ColumnCount;
            }
        }

        private void зберегтиToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                double[] d = WorkWithDataGridView.ReadFromGrid(dataGridView1);
                WorkWithTxtFiles.writeStringToFile(saveFileDialog1.FileName, d);

            }
        }

        private void вихідToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void найбільшийЕлементToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            double[] n = WorkWithDataGridView.ReadFromGrid(dataGridView1);
            double max = -1000;
            foreach (var item in n)
            {
                if (item < 0 && item > max)
                {
                    max = item;
                }
            }
            MessageBox.Show(max.ToString());
        }

        private void numericUpDown1_ValueChanged_1(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = Convert.ToInt32(numericUpDown1.Value);
        }
    }
}
