﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таблицяЕксельToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.функціїToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.знайтиСумуВсіхВитратToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.знайтиСумуВсіхДоходівToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.знайтиБюджетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вийтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.TotalExpenses = new System.Windows.Forms.Label();
            this.TotalIncome = new System.Windows.Forms.Label();
            this.Budget = new System.Windows.Forms.Label();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.функціїToolStripMenuItem,
            this.вийтиToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1062, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.таблицяЕксельToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(59, 24);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // таблицяЕксельToolStripMenuItem
            // 
            this.таблицяЕксельToolStripMenuItem.Name = "таблицяЕксельToolStripMenuItem";
            this.таблицяЕксельToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.таблицяЕксельToolStripMenuItem.Text = "ТаблицяЕксель";
            this.таблицяЕксельToolStripMenuItem.Click += new System.EventHandler(this.таблицяЕксельToolStripMenuItem_Click);
            // 
            // функціїToolStripMenuItem
            // 
            this.функціїToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.знайтиСумуВсіхВитратToolStripMenuItem,
            this.знайтиСумуВсіхДоходівToolStripMenuItem,
            this.знайтиБюджетToolStripMenuItem});
            this.функціїToolStripMenuItem.Name = "функціїToolStripMenuItem";
            this.функціїToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.функціїToolStripMenuItem.Text = "Функції";
            // 
            // знайтиСумуВсіхВитратToolStripMenuItem
            // 
            this.знайтиСумуВсіхВитратToolStripMenuItem.Name = "знайтиСумуВсіхВитратToolStripMenuItem";
            this.знайтиСумуВсіхВитратToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.знайтиСумуВсіхВитратToolStripMenuItem.Text = "Знайти суму всіх витрат";
            this.знайтиСумуВсіхВитратToolStripMenuItem.Click += new System.EventHandler(this.знайтиСумуВсіхВитратToolStripMenuItem_Click);
            // 
            // знайтиСумуВсіхДоходівToolStripMenuItem
            // 
            this.знайтиСумуВсіхДоходівToolStripMenuItem.Name = "знайтиСумуВсіхДоходівToolStripMenuItem";
            this.знайтиСумуВсіхДоходівToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.знайтиСумуВсіхДоходівToolStripMenuItem.Text = "Знайти суму всіх доходів";
            this.знайтиСумуВсіхДоходівToolStripMenuItem.Click += new System.EventHandler(this.знайтиСумуВсіхДоходівToolStripMenuItem_Click);
            // 
            // знайтиБюджетToolStripMenuItem
            // 
            this.знайтиБюджетToolStripMenuItem.Name = "знайтиБюджетToolStripMenuItem";
            this.знайтиБюджетToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.знайтиБюджетToolStripMenuItem.Text = "Знайти бюджет";
            this.знайтиБюджетToolStripMenuItem.Click += new System.EventHandler(this.знайтиБюджетToolStripMenuItem_Click);
            // 
            // вийтиToolStripMenuItem
            // 
            this.вийтиToolStripMenuItem.Name = "вийтиToolStripMenuItem";
            this.вийтиToolStripMenuItem.Size = new System.Drawing.Size(69, 24);
            this.вийтиToolStripMenuItem.Text = "Вийти ";
            this.вийтиToolStripMenuItem.Click += new System.EventHandler(this.вийтиToolStripMenuItem_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(61, 51);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(453, 300);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Total Costs";
            this.chart1.Titles.Add(title1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 413);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(232, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Записати Витрати";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(622, 411);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(241, 46);
            this.button2.TabIndex = 3;
            this.button2.Text = "Записати Доходи ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TotalExpenses
            // 
            this.TotalExpenses.AutoSize = true;
            this.TotalExpenses.Location = new System.Drawing.Point(297, 486);
            this.TotalExpenses.Name = "TotalExpenses";
            this.TotalExpenses.Size = new System.Drawing.Size(131, 16);
            this.TotalExpenses.TabIndex = 4;
            this.TotalExpenses.Text = "Сумарні Витрати = ";
            // 
            // TotalIncome
            // 
            this.TotalIncome.AutoSize = true;
            this.TotalIncome.Location = new System.Drawing.Point(692, 486);
            this.TotalIncome.Name = "TotalIncome";
            this.TotalIncome.Size = new System.Drawing.Size(119, 16);
            this.TotalIncome.TabIndex = 5;
            this.TotalIncome.Text = "Сумарний дохід =";
            // 
            // Budget
            // 
            this.Budget.AutoSize = true;
            this.Budget.Location = new System.Drawing.Point(521, 456);
            this.Budget.Name = "Budget";
            this.Budget.Size = new System.Drawing.Size(71, 16);
            this.Budget.TabIndex = 6;
            this.Budget.Text = "Бюджет = ";
            // 
            // chart2
            // 
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(593, 51);
            this.chart2.Name = "chart2";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart2.Series.Add(series2);
            this.chart2.Size = new System.Drawing.Size(446, 299);
            this.chart2.TabIndex = 7;
            this.chart2.Text = "chart2";
            title2.Name = "Title1";
            title2.Text = "Total Income";
            this.chart2.Titles.Add(title2);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 550);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.Budget);
            this.Controls.Add(this.TotalIncome);
            this.Controls.Add(this.TotalExpenses);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Графік витрат та доходів";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem функціїToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem знайтиСумуВсіхДоходівToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem знайтиСумуВсіхВитратToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вийтиToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem знайтиБюджетToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label TotalExpenses;
        private System.Windows.Forms.Label TotalIncome;
        private System.Windows.Forms.Label Budget;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.ToolStripMenuItem таблицяЕксельToolStripMenuItem;
    }
}

