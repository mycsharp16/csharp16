﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        Chart chart2;
        private Dictionary<string, NumericUpDown> numericUpDowns;
        private Color[] categoryColors;
        public Form3(Chart chart)
        {
            InitializeComponent();
            InitializeNumericUpDowns();
            InitializeCategoryColors();
            chart2 = chart;
        }

        private void InitializeNumericUpDowns()
        {
            numericUpDowns = new Dictionary<string, NumericUpDown>
            {
                { "Salary", numericUpDown1 },
                { "Premium", numericUpDown2 },
                { "gifts", numericUpDown3 },
            };
        }

        private void InitializeCategoryColors()
        {
            categoryColors = new Color[]
            {
                Color.Green,
                Color.Orange,
                Color.Purple
            };
        }

        private void AddSeries(string category, decimal value)
        {
            Series series = chart2.Series.Add(category);
            series.ChartType = SeriesChartType.Column;
            series.Points.AddXY("", value);
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void EnterIncome_Click(object sender, EventArgs e)
        {
            chart2.Series.Clear();

            AddSeries("Salary", numericUpDown1.Value);
            AddSeries("Premium", numericUpDown2.Value);
            AddSeries("gifts", numericUpDown3.Value);

            SetSeriesColors();

            Close();
        }

        private void SetSeriesColors()
        {
            Color[] colors = {Color.Green, Color.Orange, Color.Purple };

            for (int i = 0; i < chart2.Series.Count; i++)
            {
                if (i < colors.Length)
                    chart2.Series[i].Color = colors[i];
                else
                    chart2.Series[i].Color = Color.Black;
            }
        }
    }
}
