﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        Chart chart1;
        private Dictionary<string, NumericUpDown> numericUpDowns;
        private Color[] categoryColors;
        

        public Form2(Chart chart)
        {
            InitializeComponent();
            InitializeNumericUpDowns();
            InitializeCategoryColors();
            chart1 = chart;
        }

        private void InitializeNumericUpDowns()
        {
            numericUpDowns = new Dictionary<string, NumericUpDown>
            {
                { "Products", numericUpDown1 },
                { "Clothes", numericUpDown2 },
                { "Vacation", numericUpDown3 },
                { "Transport", numericUpDown4 },
                { "Communal House", numericUpDown5 }
            };
        }

        private void InitializeCategoryColors()
        {
            categoryColors = new Color[]
            {
                Color.Red,
                Color.Blue,
                Color.Green,
                Color.Orange,
                Color.Purple
            };
        }
        private void AddSeries(string category, decimal value)
        {
            Series series = chart1.Series.Add(category);
            series.ChartType = SeriesChartType.Column;
            series.Points.AddXY("", value);
        }

        private void EnterExpenses_Click(object sender, EventArgs e)
        {

            chart1.Series.Clear();

            AddSeries("Products", numericUpDown1.Value);
            AddSeries("Clothes", numericUpDown2.Value);
            AddSeries("Vacation", numericUpDown3.Value);
            AddSeries("Transport", numericUpDown4.Value);
            AddSeries("Communal House", numericUpDown5.Value);

            SetSeriesColors();

            Close();
        }

        private void SetSeriesColors()
        {
            Color[] colors = { Color.Red, Color.Blue, Color.Green, Color.Orange, Color.Purple };

            for (int i = 0; i < chart1.Series.Count; i++)
            {
                if (i < colors.Length)
                    chart1.Series[i].Color = colors[i];
                else
                    chart1.Series[i].Color = Color.Black;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
