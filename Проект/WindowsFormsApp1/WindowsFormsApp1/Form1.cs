﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2(chart1);
            frm2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3(chart2);
            frm3.ShowDialog();
        }

        private void вийтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void знайтиСумуВсіхВитратToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                decimal sum = CalculateChartSum(chart1);

                TotalExpenses.Text = "Сумарні Витрати =  " + sum.ToString();
            }

            decimal CalculateChartSum(Chart chart)
            {
                decimal sum = 0;

                foreach (Series series in chart.Series)
                {
                    foreach (DataPoint dataPoint in series.Points)
                    {
                        sum += (decimal)dataPoint.YValues[0];
                    }
                }

                return sum;
            }
        }

        private void знайтиСумуВсіхДоходівToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                decimal sum = CalculateChartSum(chart2);

                TotalIncome.Text = "Сумарний дохід = " + sum.ToString();
            }

            decimal CalculateChartSum(Chart chart)
            {
                decimal sum = 0;

                foreach (Series series in chart.Series)
                {
                    foreach (DataPoint dataPoint in series.Points)
                    {
                        sum += (decimal)dataPoint.YValues[0];
                    }
                }

                return sum;
            }
        }

        private void знайтиБюджетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                decimal sum1 = CalculateChartSum(chart1);
                decimal sum2 = CalculateChartSum(chart2);

                decimal totalSum = sum2 - sum1;

                string sign = totalSum >= 0 ? "+" : "-";
                decimal absoluteSum = Math.Abs(totalSum);

                Budget.Text = "Бюджет  " + sign + absoluteSum.ToString();
            }

            decimal CalculateChartSum(Chart chart)
            {
                decimal sum = 0;

                foreach (Series series in chart.Series)
                {
                    foreach (DataPoint dataPoint in series.Points)
                    {
                        sum += (decimal)dataPoint.YValues[0];
                    }
                }

                return sum;
            }
        }

        private void таблицяЕксельToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4(chart1,chart2);
            frm4.ShowDialog();
        }
    }
}