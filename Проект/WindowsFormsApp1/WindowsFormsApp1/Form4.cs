﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace WindowsFormsApp1
{
    public partial class Form4 : Form
    {
        Chart chart1;
        Chart chart2;
        public Form4(Chart chart1,Chart chart2)
        {
            InitializeComponent();
            this.chart1 = chart1;
            this.chart2 = chart2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = 5;
            dataGridView1.RowCount = 2;
            dataGridView1[0, 0].Value = "Products";
            dataGridView1[1, 0].Value = "Clothes";
            dataGridView1[2, 0].Value = "Vacation";
            dataGridView1[3, 0].Value = "Transport";
            dataGridView1[4, 0].Value = "Comunal";
            for (int i = 0; i < chart1.Series["Products"].Points.Count; i++)
            {
                string value = chart1.Series["Products"].Points[i].YValues[0].ToString();
                dataGridView1[0, 1].Value = value;
            }

            for (int i = 0; i < chart1.Series["Clothes"].Points.Count; i++)
            {
                string value = chart1.Series["Clothes"].Points[i].YValues[0].ToString();
                dataGridView1[1, 1].Value = value;
            }

            for (int i = 0; i < chart1.Series["Vacation"].Points.Count; i++)
            {
                string value = chart1.Series["Vacation"].Points[i].YValues[0].ToString();
                dataGridView1[2, 1].Value = value;
            }

            for (int i = 0; i < chart1.Series["Transport"].Points.Count; i++)
            {
                string value = chart1.Series["Transport"].Points[i].YValues[0].ToString();
                dataGridView1[3, 1].Value = value;
            }

            for (int i = 0; i < chart1.Series["Communal House"].Points.Count; i++)
            {
                string value = chart1.Series["Communal House"].Points[i].YValues[0].ToString();
                dataGridView1[4, 1].Value = value;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {            
            //Створюємо екземпляр додатку
            Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

            //Відображаємо додаток (не обов"язково)
            excelApp.Visible = true;

            //Задаємо кількість листів у книзі
            excelApp.SheetsInNewWorkbook = 1;

            //Створюємо робочу книгу
            Excel.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);

            Excel.Worksheet worksheet1 = workBook.Sheets.Add();
            worksheet1.Name = "Доходи";

            Excel.Worksheet worksheet2 = workBook.Sheets.Add();
            worksheet2.Name = "Витрати";

            //Відключаємо можливість відображення повідомлень
            excelApp.DisplayAlerts = false;

            //Отримуємо посилання на перший лист(нумерація від 1)
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);

            //Записуємо дані у  лист
            int offsetRow = 1;
            //Записуємо назви стовпців
            for (int j = 0; j < dataGridView1.ColumnCount; j++)
            {
                sheet.Cells[offsetRow, j + 1] = dataGridView1.Columns[j].HeaderText;
            }

            //Записуємо дані 
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j++)
                    sheet.Cells[offsetRow + i + 1, j + 1] = dataGridView1[j, i].Value;
            }



            //Отримуємо посилання на другий лист(нумерація від 1)
            Excel.Worksheet sheet1 = (Excel.Worksheet)excelApp.Worksheets.get_Item(2);
            //Записуємо дані у  лист
            int offsetRow1 = 2;
            //Записуємо назви стовпців
            for (int j = 0; j < dataGridView2.ColumnCount; j++)
            {
                sheet1.Cells[offsetRow1, j + 1] = dataGridView2.Columns[j].HeaderText;
            }

            //Записуємо дані 
            for (int i = 0; i < dataGridView2.RowCount; i++)
            {
                for (int j = 0; j < dataGridView2.ColumnCount; j++)
                    sheet1.Cells[offsetRow1 + i + 1, j + 1] = dataGridView2[j, i].Value;
            }

            //Задаємо діапазон клітинок
            Excel.Range range1 = sheet.Range[sheet.Cells[offsetRow, 1], sheet.Cells[offsetRow, dataGridView1.ColumnCount]];
            Excel.Range range2 = sheet1.Range[sheet1.Cells[offsetRow1, 1], sheet1.Cells[offsetRow1, dataGridView2.ColumnCount]];
            //Ім"я шрифту
            range1.Cells.Font.Name = "Arial";
            range2.Cells.Font.Name = "Arial";

            //Розмір шрифту
            range1.Cells.Font.Size = 14;
            range2.Cells.Font.Size = 14;

            //Колір символів
            range1.Cells.Font.Color = ColorTranslator.ToOle(Color.White);
            range2.Cells.Font.Color = ColorTranslator.ToOle(Color.White);


        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView2.ColumnCount = 3;
            dataGridView2.RowCount = 2;
            dataGridView2[0, 0].Value = "Salary";
            dataGridView2[1, 0].Value = "Premium";
            dataGridView2[2, 0].Value = "gifts";
            
            for (int i = 0; i < chart2.Series["Salary"].Points.Count; i++)
            {
                string value = chart2.Series["Salary"].Points[i].YValues[0].ToString();
                dataGridView2[0, 1].Value = value;
            }

            for (int i = 0; i < chart2.Series["Premium"].Points.Count; i++)
            {
                string value = chart2.Series["Premium"].Points[i].YValues[0].ToString();
                dataGridView2[1, 1].Value = value;
            }

            for (int i = 0; i < chart2.Series["gifts"].Points.Count; i++)
            {
                string value = chart2.Series["gifts"].Points[i].YValues[0].ToString();
                dataGridView2[2, 1].Value = value;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Створення об'єкта Excel
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook workbook = null;
            Excel.Worksheet worksheet = null;

            try
            {
                // Відкриття Excel файлу
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.Title = "Select an Excel File";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    // Отримання шляху до вибраного файлу
                    string filePath = openFileDialog.FileName;

                    // Відкриття робочої книги та робочого листа
                    workbook = excelApp.Workbooks.Open(filePath);
                    worksheet = workbook.ActiveSheet;

                    // Очищення dataGridView1
                    dataGridView1.Rows.Clear();
                    dataGridView1.Columns.Clear();

                    // Отримання значень з Excel та заповнення dataGridView1
                    int rowCount = worksheet.UsedRange.Rows.Count;
                    int columnCount = worksheet.UsedRange.Columns.Count;

                    // Додавання стовпців до dataGridView1
                    for (int col = 1; col <= columnCount; col++)
                    {
                        string columnName = Convert.ToChar('A' + col - 1).ToString();
                        dataGridView1.Columns.Add(columnName, columnName);
                    }

                    // Заповнення даними
                    for (int row = 1; row <= rowCount; row++)
                    {
                        DataGridViewRow dataGridViewRow = new DataGridViewRow();

                        for (int col = 1; col <= columnCount; col++)
                        {
                            DataGridViewTextBoxCell cell = new DataGridViewTextBoxCell();
                            cell.Value = worksheet.Cells[row, col].Value;
                            dataGridViewRow.Cells.Add(cell);
                        }

                        dataGridView1.Rows.Add(dataGridViewRow);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                // Закриття робочого листа та робочої книги, закриття Excel
                //worksheet?.Close();
                workbook?.Close();
                excelApp?.Quit();

                // Звільнення ресурсів
                releaseObject(worksheet);
                releaseObject(workbook);
                releaseObject(excelApp);
            }
        }

        // Видалення посилань на COM об'єкти та звільнення ресурсів
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

    }
}

